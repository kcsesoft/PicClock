//
//  ClockHand.cpp
//  pic_clock
//
//  Created by Toida on 2015/06/16.
//
//

#include <time.h>
#include <math.h>
#include "ClockHand.h"

USING_NS_CC;

ClockHand::ClockHand()
{
	
}

ClockHand::~ClockHand()
{
	
}

ClockHand* ClockHand::create(int hand)
{
	ClockHand* ret = new ClockHand();
	
	if(ret && ret->init(hand)){
		return ret;
	}
	else{
		CC_SAFE_DELETE(ret);
		return NULL;
	}
}

/* 初期化 */
bool ClockHand::init(int hand)
{
	struct tm		*now_date;
	time_t			now_time;
	float			hour_angle;
	float			min_angle;
	float			sec_angle;


	if(!Sprite::init()){
		return false;
	}

    Size visibleSize = Director::getInstance()->getVisibleSize();

	time(&now_time);
	now_date = localtime(&now_time);

	/* 針角度 */
	GetClockAngle(now_date, &hour_angle, &min_angle, &sec_angle);

	switch (hand) {
		case PARAM_HOUR_HAND:
			setTextureRect(Rect(0, 0, WIDTH_HOUR_HAND, HEIGHT_HOUR_HAND));
			setColor(Color3B::BLACK);
			setRotation(hour_angle);
			break;
		case PARAM_MIN_HAND:
			setTextureRect(Rect(0, 0, WIDTH_MIN_HAND, HEIGHT_MIN_HAND));
			setColor(Color3B::BLACK);
			setRotation(min_angle);
			break;
		case PARAM_SEC_HAND:
			setTextureRect(Rect(0, 0, WIDTH_SEC_HAND, HEIGHT_SEC_HAND));
			setColor(Color3B::RED);
			setRotation(sec_angle);
			break;
		default:
			break;
	}
	setPosition(Point(visibleSize.width/2, visibleSize.height/2));
	setAnchorPoint(Point(0.5f, 0.0f));
	m_hand = hand;

	return true;
}

/* 針角度更新 */
void ClockHand::ClockHandUpdate()
{
	struct tm		*now_date;
	time_t			now_time;
	float			hour_angle;
	float			min_angle;
	float			sec_angle;

	auto director = Director::getInstance();
	Size winSize =  director->getWinSize();

	time(&now_time);
	now_date = localtime(&now_time);
	
	/* 針角度 */
	GetClockAngle(now_date, &hour_angle, &min_angle, &sec_angle);

	switch (m_hand) {
		case PARAM_HOUR_HAND:
			setRotation(hour_angle);
			break;
		case PARAM_MIN_HAND:
			setRotation(min_angle);
			break;
		case PARAM_SEC_HAND:
			setRotation(sec_angle);
			break;
		default:
			break;
	}
}

/* 針角度取得 */
void ClockHand::GetClockAngle(struct tm *t, float *hour_angle, float *min_angle, float *sec_angle)
{
	float hour;
	float min;
	float sec;
	int time_hour;


	time_hour = t->tm_hour;
	if (time_hour >= 12){
		time_hour -= 12;
	}
	
	/* 時 */
	hour = ((float)time_hour * 30.0f) + ((float)t->tm_min / 2.0f);
	
	/* 分 */
	min = (float)t->tm_min * 6.0f;
	
	/* 秒 */
	sec = (float)t->tm_sec * 6.0f;
	
	*hour_angle = hour;
	*min_angle = min;
	*sec_angle = sec;
	
	return;
}

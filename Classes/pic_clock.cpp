#include <time.h>
#include <math.h>
#include "pic_clock.h"
#include "ClockHand.h"
//#include "Config.h"

USING_NS_CC;

Scene* PicClock::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PicClock::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PicClock::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(PicClock::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...
	int center_x;
	int center_y;

	/* 画面サイズ取得 */
	auto director = Director::getInstance();
	Size winSize =  director->getWinSize();

	center_x = winSize.width / 2;
	center_y = winSize.height / 2;

	/* 盤面 */
	auto sprite_dial = Sprite::create("dials.png");
	sprite_dial->setPosition(Point(visibleSize.width/2, visibleSize.height/2));
	this->addChild(sprite_dial, ZORDER_DIAL, ITAG_DIAL);
	
	/* 時 */
	ClockHand* sprite_hour = ClockHand::create(PARAM_HOUR_HAND);
	this->addChild(sprite_hour, ZORDER_HOUR_HAND, ITAG_HOUR_HAND);
	
	/* 分 */
	ClockHand* sprite_min = ClockHand::create(PARAM_MIN_HAND);
	this->addChild(sprite_min, ZORDER_MIN_HAND, ITAG_MIN_HAND);

	/* 秒 */
	ClockHand* sprite_sec = ClockHand::create(PARAM_SEC_HAND);
	this->addChild(sprite_sec, ZORDER_SEC_HAND, ITAG_SEC_HAND);

	/* 中央 */
	DrawNode *ceter_node = DrawNode::create();
	ceter_node->drawDot(Vec2(center_x, center_y), 11, Color4F(Color4F::BLACK));
	this->addChild(ceter_node, ZORDER_CENTER, ITAG_CENTER);

	DrawNode *ceter2_node = DrawNode::create();
	ceter2_node->drawDot(Vec2(center_x, center_y), 10, Color4F(Color4F::YELLOW));
	this->addChild(ceter2_node, ZORDER_CENTER, ITAG_CENTER);

	/* スケジューラ */
	this->schedule(schedule_selector(PicClock::update), 1.0f);

    return true;
}

void PicClock::update(float delta)
{
	/* 時 */
	ClockHand* sprite_hour = (ClockHand*)this->getChildByTag(ITAG_HOUR_HAND);
	sprite_hour->ClockHandUpdate();

	/* 分 */
	ClockHand* sprite_min = (ClockHand*)this->getChildByTag(ITAG_MIN_HAND);
	sprite_min->ClockHandUpdate();
	
	/* 秒 */
	ClockHand* sprite_sec = (ClockHand*)this->getChildByTag(ITAG_SEC_HAND);
	sprite_sec->ClockHandUpdate();

	return;
	
}

void PicClock::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
